Rails.application.routes.draw do
  
  get 'users/show' => 'users#show'
  get 'users/new' => 'users#new'
  post 'users/login' => 'users#login'
  get 'users/login' => 'users#login_form'
  get 'users/logout' => 'users#logout'
  post 'users/create' => 'users#create'
  get 'users/:id/edit' => 'users#edit'
  post 'users/:id/update' => 'users#update'
  get 'users/:id' => 'users#detail'
  post 'users/:id/del' => 'users#del'

  get 'events' => 'events#index'
  get 'events/new' => 'events#new'
  post 'events/new' => 'events#create'
  get 'events/:id/edit' => 'events#edit'
  post 'events/:id/edit' => 'events#update'
  post 'events/:id/del' => 'events#del'
  get 'events/:id' => 'events#detail'

  post 'entry/add' => 'entry#add'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
