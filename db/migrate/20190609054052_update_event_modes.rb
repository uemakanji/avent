class UpdateEventModes < ActiveRecord::Migration[5.2]
  def change
    change_column_null :events, :title, false
  end
end
