class AddUserIdContinue < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :user_id, :integer, null: false
  end
end
