class UserIdUnique < ActiveRecord::Migration[5.2]
  def change
    add_index :users, :user_id
    change_column_null :users, :user_id, false
  end
end
