class EventUserIdChangeBigint < ActiveRecord::Migration[5.2]
  def change
    change_column :events, :user_id, :bigint, null: false
    #Ex:- change_column("admin_users", "email", :string, :limit =>25)
  end
end
