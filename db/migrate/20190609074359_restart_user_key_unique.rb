class RestartUserKeyUnique < ActiveRecord::Migration[5.2]
  def change
    add_index :users, :user_id, unique: true
    change_column_null :users, :user_id, false
  end
end
