class ChangeOption < ActiveRecord::Migration[5.2]
  def change
    change_column :users, :name, :string, null: false
    change_column :users, :d_name, :string, null: false, default: "general"
    change_column :users, :password_digest, :string, null: false
  end
end
