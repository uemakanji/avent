class ChangeTimeToDatetime < ActiveRecord::Migration[5.2]
  def change
    change_column :events, :stime, :datetime
    change_column :events, :etime, :datetime
  end
end
