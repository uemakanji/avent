class EventUserNotNull < ActiveRecord::Migration[5.2]
  def change
    change_column :entries, :event_id, :bigint, null: false
    change_column :entries, :user_id, :bigint, null: false
  end
end
