class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name, :null => false
      t.string :d_name
      t.string :role, :null => false, :default => "general"
      t.string :password_digest :null => false

      t.timestamps
    end
  end
end
