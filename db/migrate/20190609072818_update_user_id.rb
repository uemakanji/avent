class UpdateUserId < ActiveRecord::Migration[5.2]
  def change
    remove_index :users, :user_id
  end
end
