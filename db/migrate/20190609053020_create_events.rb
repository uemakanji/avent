class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :title
      t.string :place
      t.string :text
      t.time :stime
      t.time :etime

      t.timestamps
    end
  end
end
