class Event < ApplicationRecord
    validates :title, presence: true, length: {maximum: 255}
    validates :place, length: {maximum: 255}
    validates :text, length: {maximum:255}
end
