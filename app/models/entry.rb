class Entry < ApplicationRecord
    validates :event_id, presence: true
    validates :user_id, presence: true
end
