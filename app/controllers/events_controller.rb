class EventsController < ApplicationController
    before_action :check_nil, only: [:index, :new, :edit, :remove]

    #イベント一覧表示
    def index
        @events = Event.all
    end

    #新規イベント作成
    def new
    end

    def create
        @event = Event.new(title: params[:title], place: params[:place], text: params[:text],
            stime: params[:stime], etime: params[:etime], user_id: params[:user_id])
        if @event.save()
            flash[:notice] = "作成しました"
            redirect_to('/events')
        end
    end

    #イベント編集
    def edit
        @event = Event.find(params[:id])
    end

    def update
        @event = Event.find(params[:id])
        @event.title = params[:title]
        @event.place = params[:place]
        @event.text = params[:text]
        @event.stime = params[:stime]
        @event.etime = params[:etime]
        if @event.save()
            flash[:notice] = "更新しました"
            redirect_to('/events')
        end
    end

    #イベント削除
    def remove
        @event = Event.find(params[:id])
    end

    def del
        Event.find(params[:id]).delete
        flash[:notice] = "削除しました"
        redirect_to('/events')
    end

    def detail
        @event = Event.find(params[:id])
        entrys = Entry.where(event_id: params[:id])
        ids = []
        entrys.each do |entry| 
            ids << entry.user_id
        end
        @users = User.find(ids)
    end

end
