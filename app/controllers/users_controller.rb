class UsersController < ApplicationController
  before_action :check_nil, only: [:detail, :edit, :update, :del]
  before_action :check_user, only: [:detail, :edit, :update, :del]
  before_action :login_user, only: [:login, :login_form]

  #ユーザ一覧
  def show
    @users = User.all
  end

  #登録
  def new
  end

  #登録処理
  def create
    @user = User.new(name: params[:name], d_name: params[:d_name], password: params[:password],
      password_confirmation: params[:password], user_id: params[:user_id])
    @user.save
    redirect_to("/users/#{@user.id}")
  end

  #ユーザ詳細
  def detail
    @user = User.find(params[:id])
  end

  #ログイン
  def login_form
  end
  
  #ログイン処理
  def login
    @user = User.find_by(user_id: params[:user_id])
    if @user && @user.authenticate(params[:password])
      session[:id] = @user.id
      flash[:notice] = "ログインしました"
      redirect_to("/users/#{session[:id]}")
      return
    else
      flash[:notice] = "ユーザIDまたはパスワードが間違っています"
      render('users/login_form')
      return
    end
  end

  def logout
    session[:id] = nil
    flash[:notice] = "ログアウトしました"
    redirect_to('/users/login')
  end

  def edit
    @user = User.find_by(id: params[:id])
  end

  def update
    @user = User.find_by(id: params[:id])
    @user.user_id = params[:user_id]
    @user.name = params[:name]
    @user.d_name = params[:d_name]

    if @user.save
      flash[:notice] = "変更しました"
      redirect_to("/users/#{ @user.id }")
    end
  end

  def del
    User.find(params[:id]).delete
    flash[:notice] = "削除しました"
    redirect_to('/users/login')
  end
end
