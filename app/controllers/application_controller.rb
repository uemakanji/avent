class ApplicationController < ActionController::Base
    before_action :set_current_user

    def set_current_user
        @current_user = User.find_by(id: session[:id])
    end
    
    def check_user
        if @current_user.id != params[:id].to_i
            redirect_to("/users/login")
        end
    end

    def check_nil
        if @current_user == nil
            redirect_to("/users/login")
        end
    end

    def login_user
        if @current_user
            flash[:notice] = "すでにログインしています"
            redirect_to("/users/#{@current_user.id}")
        end
    end
end
