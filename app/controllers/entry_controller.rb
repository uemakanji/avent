class EntryController < ApplicationController
    def add
        @entry = Entry.new(event_id: params[:event_id], user_id: @current_user.id)
        if @entry.save()
            flash[:notice] = "参加登録しました"
            redirect_to('/events/#{params[:event_id]}')
        end
    end
end
